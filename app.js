const express     = require("express");
const bodyParser  =require("body-parser");
const app         = express();
const mongoose    = require("mongoose");

// const actors = require('./data/actors');
// const photos = require('./data/photos');

mongoose.connect("mongodb://localhost/koko", { useNewUrlParser: true }) ;
app.use(express.static("public"));
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true}));

// Schema Setup
var photoSchema = new mongoose.Schema({
  alt: String, 
  pic: String, 
  gridType: String
});
var personSchema = new mongoose.Schema({
  castType: String,
    crewType: String,
    name: String,
    playing: String,
    picLocate: String,
    href: String,
    description: String,
});

var photo = mongoose.model("Photo", photoSchema);
var person = mongoose.model("Person", personSchema);
// photo.create(
//   {
//     alt: "Dr. Kinkle and Friend", pic: "courthouse/Dr. Kinkle and Friend.jpg" , gridType: "7" 
//   }, function(err, photo) {
//     if(err) {
//       console.log(err);
//     } else {
//       console.log("New Photo added");
//       console.log(photo);
//     }
//   });

//main site
app.get("/", function(req, res){
  res.render("site/home", {actors: actors});
});
 
// app.get("/cast/:person", function(req, res){
//   var person = req.params.person;
//   // res.send("this page is for a " + person);
//   res.render("site/starring", {person: person, actors: actors});
// });

// app.get("/cast-crew", function(req, res){
//   res.render("site/cast-crew1", {actors: actors});
// });

//Persons
app.get("/credits", function(req, res) {
  person.find({}, function(err, persons) {
    if(err) {
      console.log(err);
    } else {
      res.render("site/persons", {persons: persons});
    }
  });
});
app.get("/credits/new", function(req, res){
  res.render("site/newPerson");
});
app.post("/credits/addperson", function(req, res){
  person.create(req.body.person, function(err, NewCreate) {
    if(err) {
      console.log(err);
    } else {
      res.redirect("/credits");
    }
  })
});
app.get("/credits/:id", function(req, res){
  // var id = req.params.id;
  person.findById(req.param.id, function(err, foundPerson){
    if(err) {
      res.redirect("/persons")
    } else { 
      // res.render("site/starring", {person: foundPerson})
      console.log(foundPerson);
    }
  })
});

// Photos
app.get("/photos", function(req, res){
  photo.find({}, function(err, allphotos) {
    if(err) {
      console.log(err);
    } else {
      res.render("site/photos", {photos: allphotos});
    }
  })
});
app.post("/photos/addphoto", function(req, res){
  var alt = req.body.alt; 
  var pic = req.body.pic; 
  var gridType= req.body.gridType;
  var newPhoto = {alt:alt, pic:pic, gridType:gridType}
  photo.create(newPhoto,function(err, NewCreated) {
    if(err) {
      console.log(err);
    } else {
      res.redirect("/photos");
    }
  })
});
app.get("/photos/new", function(req, res) {
  res.render("site/newPhoto" );
});
app.get("/photos/:id", function(req, res) {
  photo.findById(req.params.id, function(err, foundPhoto) {
    if(err) {
      console.log(err);
    } else {
      res.render("site/photo", {photo: foundPhoto});
    }
  })
});

app.get("/about", function(req, res){
  res.render("site/about", );
});






app.get("*", function(req,res) {
  res.send("You are a star!")
});

app.listen(3000, function(){
  console.log("Server has started!!!")
});

module.export = app;
